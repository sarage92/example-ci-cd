apt update && apt install -y zlib1g-dev g++ git libicu-dev zip libzip-dev zip \
    && docker-php-ext-install intl opcache pdo pdo_mysql \
    && pecl install apcu \
    && pecl install grpc \
    && docker-php-ext-enable apcu \
    && docker-php-ext-enable grpc \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip

WORKDIR /var/www

 curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
 curl -sS https://get.symfony.com/cli/installer | bash
 mv /root/.symfony/bin/symfony /usr/local/bin/symfony